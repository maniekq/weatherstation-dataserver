package pl.aetas.controller

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/general")
class GeneralController {

    @RequestMapping("/register/{deviceId}")
    fun registerDevice() {
        println("New device registered")
    }
}