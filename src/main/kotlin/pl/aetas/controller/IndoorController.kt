package pl.aetas.controller

import org.eclipse.paho.client.mqttv3.IMqttClient
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import pl.aetas.indoor.IndoorData
import pl.aetas.indoor.IndoorDataRepository
import pl.aetas.indoor.IndoorResource
import pl.aetas.mqtt.MqttBroker
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.temporal.ChronoField
import java.time.temporal.ChronoUnit

@RestController
@RequestMapping("/indoor")
class IndoorController @Autowired constructor(val repository: IndoorDataRepository, val mqttClient: IMqttClient) {

    companion object {
        val log = LoggerFactory.getLogger(IndoorController::class.java)!!
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun addReading(@RequestBody input: IndoorResource) {
        log.debug("Input received: $input")
        repository.save(IndoorData(input.temperature, input.humidity))

        MqttBroker(mqttClient).use {
            it.send("weatherstation-bedroom/temperature", input.temperature)
            it.send("weatherstation-bedroom/humidity", input.humidity)
        }
    }

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun getLatest(): IndoorResource {
        return toResource(repository.findFirstByOrderByDateDesc())
    }

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/history"))
    fun getAll(): Iterable<IndoorResource> {
        val allWeatherData = repository.findAll()
        return allWeatherData.map { toResource(it) }
    }

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/history"), params = arrayOf("period=24h"))
    fun lastDayData(): Iterable<IndoorResource> {
        val startDate = Instant.now().minus(24L, ChronoUnit.HOURS);
        val endDate = Instant.now();
        return repository.findAllByDateBetween(startDate, endDate)
                .groupBy { LocalDateTime.ofInstant(it.date, ZoneId.systemDefault()).get(ChronoField.HOUR_OF_DAY) }
                .mapValues { it.value.first() }
                .values
                .map { toResource(it) }

    }

    private fun toResource(indoor: IndoorData) =
            IndoorResource(indoor.temperature, indoor.humidity, indoor.date.toEpochMilli())
}