package pl.aetas.controller

import ch.rasc.darksky.DsClient
import ch.rasc.darksky.model.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pl.aetas.outdoor.OutdoorResource
import pl.aetas.outdoor.WeatherIconResource
import java.time.Instant
import java.time.temporal.ChronoUnit

@RestController
@RequestMapping("/outdoor")
class OutdoorController @Autowired constructor(private val darkSkyApi: DsClient) {

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun currentWeather(@RequestParam("latitude") latitude: String,
                       @RequestParam("longitude") longitude: String): OutdoorResource {

        @Suppress("INACCESSIBLE_TYPE")
        val dsRequest: DsForecastRequest = DsForecastRequest.builder()
                .latitude(latitude)
                .longitude(longitude)
                .excludeBlock(DsBlock.ALERTS, DsBlock.MINUTELY, DsBlock.HOURLY)
                .unit(DsUnit.SI)
                .build()

        val dsResponse = darkSkyApi.sendForecastRequest(dsRequest)
        return toResource(dsResponse.currently())
    }

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/forecast"), params = arrayOf("period=12h"))
    fun forecast(@RequestParam("latitude") latitude: String,
                 @RequestParam("longitude") longitude: String): List<OutdoorResource> {

        @Suppress("INACCESSIBLE_TYPE")
        val dsRequest: DsForecastRequest = DsForecastRequest.builder()
                .latitude(latitude)
                .longitude(longitude)
                .excludeBlock(DsBlock.ALERTS, DsBlock.MINUTELY, DsBlock.DAILY)
                .unit(DsUnit.SI)
                .build()

        val dsResponse = darkSkyApi.sendForecastRequest(dsRequest)
        return dsResponse.hourly().data()
                .map { toResource(it) }
                .filter { Instant.ofEpochMilli(it.time).isBefore(Instant.now().plus(12L, ChronoUnit.HOURS)) }
    }

    private fun toResource(dsDataPoint: DsDataPoint): OutdoorResource {
        return OutdoorResource(
                dsDataPoint.temperature().toFloat(),
                dsDataPoint.pressure().toFloat(),
                dsDataPoint.windSpeed().toFloat(),
                toIconResource(dsDataPoint.icon()),
                dsDataPoint.precipProbability().toFloat(),
                Instant.ofEpochSecond(dsDataPoint.time()).toEpochMilli())
    }


    private fun toIconResource(dsIcon: DsIcon): WeatherIconResource {
        return when(dsIcon) {

            DsIcon.CLEAR_DAY -> WeatherIconResource.ClearDay
            DsIcon.CLEAR_NIGHT -> WeatherIconResource.ClearNight
            DsIcon.RAIN -> WeatherIconResource.Rain
            DsIcon.SNOW -> WeatherIconResource.Snow
            DsIcon.SLEET -> WeatherIconResource.Sleet
            DsIcon.WIND -> WeatherIconResource.Wind
            DsIcon.FOG -> WeatherIconResource.Fog
            DsIcon.CLOUDY -> WeatherIconResource.Cloudy
            DsIcon.PARTLY_CLOUDY_DAY -> WeatherIconResource.PartlyCloudyDay
            DsIcon.PARTLY_CLOUDY_NIGHT -> WeatherIconResource.PartlyCloudyNight
            else -> WeatherIconResource.Other
        }
    }

}