package pl.aetas.outdoor

import com.fasterxml.jackson.annotation.JsonInclude
import java.time.Instant

@JsonInclude(JsonInclude.Include.NON_EMPTY)
data class OutdoorResource(val temperature: Float,
                           val pressure: Float,
                           val windSpeed: Float,
                           val icon: WeatherIconResource,
                           val precipitationProbability: Float,
                           val time: Long)

enum class WeatherIconResource {
    ClearDay, ClearNight, Rain, Snow, Sleet, Wind, Fog, Cloudy, PartlyCloudyDay, PartlyCloudyNight, Other
}