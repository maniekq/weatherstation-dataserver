package pl.aetas.outdoor

import ch.rasc.darksky.DsClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class OutdoorConfig @Autowired constructor(val darkSkyApiConfiguration: DarkSkyApiConfiguration) {

    @Bean
    open fun darkskyApi(): DsClient = DsClient(darkSkyApiConfiguration.apiKey)

}
