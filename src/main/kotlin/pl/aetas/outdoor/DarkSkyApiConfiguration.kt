package pl.aetas.outdoor

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component


@Component
@ConfigurationProperties("aetas.weatherstation.darksky")
class DarkSkyApiConfiguration {

    var apiKey: String = ""
}