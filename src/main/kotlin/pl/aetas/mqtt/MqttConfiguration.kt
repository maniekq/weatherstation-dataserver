package pl.aetas.mqtt

import org.eclipse.paho.client.mqttv3.MqttClient
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component

@Configuration
open class MqttBeans {

    @Bean
    open fun mqttClient(configuration: MqttConfiguration): MqttClient {
        return MqttClient("tcp://${configuration.host}:${configuration.port}", configuration.clientId)
    }
}

@Component
@ConfigurationProperties(prefix = "mqtt.client")
open class MqttConfiguration {
    var host: String = "";
    var port: String = "";
    var clientId: String = "";
}
