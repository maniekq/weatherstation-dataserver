package pl.aetas.mqtt

import org.eclipse.paho.client.mqttv3.IMqttClient
import org.eclipse.paho.client.mqttv3.MqttMessage
import java.io.Closeable

class MqttBroker constructor(val mqttClient: IMqttClient) : Closeable {

    init {
        mqttClient.connect()
    }

    override fun close() {
        mqttClient.disconnect()
    }



    fun send(topic: String, value: Float) {
        mqttClient.publish(topic, MqttMessage(value.toString().toByteArray()))
    }


}

