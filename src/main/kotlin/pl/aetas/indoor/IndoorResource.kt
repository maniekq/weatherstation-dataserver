package pl.aetas.indoor

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_EMPTY)
data class IndoorResource(val temperature: Float, val humidity: Float, val date: Long?)

