package pl.aetas.indoor

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.time.Instant

@Repository
interface IndoorDataRepository : CrudRepository<IndoorData, Long> {
    fun findAllByDateBetween(start: Instant, end: Instant): Iterable<IndoorData>
    fun findFirstByOrderByDateDesc(): IndoorData
}