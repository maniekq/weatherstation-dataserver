package pl.aetas.indoor

import java.time.Instant
import javax.persistence.*

@Entity
@Table(name = "WeatherData")
class IndoorData(var temperature: Float = -1F,
                 var humidity: Float = -1F,
                 var date: Instant = Instant.now(),
                 @Id @GeneratedValue(strategy = GenerationType.AUTO)
                  var id: Long = 0)