package pl.aetas.mqtt

import spock.util.concurrent.BlockingVariable

class BlockingWaiting {
  BlockingVariable<Boolean> blockingVariable = new BlockingVariable<Boolean>(5)

  void block() {
    blockingVariable.get()
  }

  void unblock() {
    blockingVariable.set(true)
  }
}