package pl.aetas.mqtt

import io.moquette.server.Server
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttMessage
import spock.lang.Specification

class MqttBrokerTest extends Specification {

  def client = new MqttClient("tcp://localhost:1880", "client123");
  def server = new Server()

  void setup() {
    def properties = new Properties()
    properties.setProperty("port", "1880")
    server.startServer(properties)
  }

  void cleanup() {
    client.disconnect()
    server.stopServer()
  }

  def "should send message to MQTT"() {
    given:
    def broker = new MqttBroker(client)
    def blocking = new BlockingWaiting()
    def callbackResult = new MqttBlockingCallback(blocking)
    client.subscribe("testTopic/1")
    client.setCallback(callbackResult)
    when:
    broker.send("testTopic/1", 13.123F)
    then:
    blocking.block()
    callbackResult.lastMessage == "13.123"
  }

  class MqttBlockingCallback implements MqttCallback {

    String lastMessage;
    private final BlockingWaiting unblocker

    MqttBlockingCallback(BlockingWaiting unblocker) {
      this.unblocker = unblocker
    }

    @Override
    void connectionLost(Throwable cause) {

    }

    @Override
    void messageArrived(String topic, MqttMessage message) throws Exception {
      lastMessage = new String(message.getPayload())
      println "Got Message in topic $topic: ${lastMessage}"
      unblocker.unblock()
    }

    @Override
    void deliveryComplete(IMqttDeliveryToken token) {

    }
  }
}
